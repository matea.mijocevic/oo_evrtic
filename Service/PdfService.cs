﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;
using BaseLib.Service;
using DataAccessLayer;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Model.Repositories;

namespace Service
{
    public class PdfService : IOutputFileService
    {
        private static IOutputFileService _service;
        private readonly IChildRepository _childRepository = ChildRepository.GetInstance();
        private readonly IEmployeeRepository _employeeRepository = EmployeeRepository.GetInstance();

        private readonly BaseFont _font = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, false);
        private const int FontSize = 12;
        private const string FileName = "Zapis";
        private const string Ext = ".pdf";
        private const int MarginSize = 10;

        public static IOutputFileService ProvidePdfService()
        {
            return _service ?? (_service = new PdfService());
        }

        public bool OutputFile()
        {

            var folderBrowserDialog = FolderBrowserDialogService.GetInstance();

            if (folderBrowserDialog.ShowDialog() != DialogResult.OK) return false;

            var filePath = folderBrowserDialog.GetPath();

            var document = new Document(PageSize.A4, MarginSize, MarginSize, MarginSize, MarginSize);
            var writer = PdfWriter.GetInstance(document, new FileStream(GetFilePathAndName(filePath), FileMode.Create));

            var regularFont = new Font(_font, FontSize, Font.NORMAL);


            var table = new PdfPTable(3) { WidthPercentage = 100 };

            var allChildren = _childRepository.GetAll();// WithRequiredPayments();

            var boldFont = new Font(_font, FontSize, Font.BOLD);
            
            table.AddCell(new PdfPCell(new Phrase("Ime", boldFont)));
            table.AddCell(new PdfPCell(new Phrase("Prezime", boldFont)));
            table.AddCell(new PdfPCell(new Phrase("Grupa", boldFont)));

            foreach (var child in allChildren)
            {
                table.AddCell(new PdfPCell(new Phrase(child.Name, regularFont)));
                table.AddCell(new PdfPCell(new Phrase(child.Surname, regularFont)));
                table.AddCell(new PdfPCell(new Phrase(child.Groupa, regularFont)));
            }

            document.Open();
            document.Add(table);

            document.Close();
            writer.Close();

            return true;
        }


        private string GetFilePathAndName(string path)
        {
            var fileNumber = 0;
            while (File.Exists(path + FileName + fileNumber + Ext))
            {
                fileNumber++;
            }

            return path + FileName + fileNumber + Ext;
        }     
    }
}
