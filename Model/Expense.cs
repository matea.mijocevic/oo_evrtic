﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Expense
    {
        public virtual int Id { get; set; }
        public virtual Child Child { get; set; }
        public virtual float Amount { get; set; }
        public virtual int Month { get; set; }
        public virtual int Year { get; set; }
        public virtual bool IsPayed { get; set; }

        public Expense()
        {
        }

        public Expense(Child child, float amount, int month, int year, bool isPayed)
        {
            Child = child;
            Amount = amount;
            Month = month;
            Year = year;
            IsPayed = isPayed;
        }
    }
}
