﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Event
    {
        public virtual int Id { get; set; }
        public virtual string EventType { get; set; }
        public virtual IList<string> Groups { get; set; }
        public virtual string Name { get; set; }
        public virtual float Amount { get; set; }
        public virtual string About { get; set; }
        public virtual string Address { get; set; }
        public virtual DateTime StartingDate { get; set; }
        public virtual DateTime EndingDate { get; set; }

        public Event()
        {
        }

        public Event(string eventType, IList<string> groups, string name, float amount, string about, string address, DateTime startingDate, DateTime endingDate)
        {
            EventType = eventType;
            Groups = groups;
            Name = name;
            Amount = amount;
            About = about;
            Address = address;
            StartingDate = startingDate;
            EndingDate = endingDate;
        }
    }
}
