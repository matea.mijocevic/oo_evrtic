﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Employee
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual string Groupa { get; set; }
        public virtual string Email { get; set; }
        public virtual string Password { get; set; }

        public virtual IList<EmployeeLog> EmployeeLogs { get; set; }
        public virtual IList<Purchase> Purchases { get; set; }

        public Employee()
        {
        }

        public Employee(string name, string surname, string groupa, string email, string password)
        {
            Name = name;
            Surname = surname;
            Groupa = groupa;
            Email = email;
            Password = password;
        }
    }
}
