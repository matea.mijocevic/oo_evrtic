﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Purchase
    {
        public virtual int Id { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual float Amount { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }

        public Purchase()
        {
        }

        public Purchase(Employee employee, float amount, string name, string description)
        {
            Employee = employee;
            Amount = amount;
            Name = name;
            Description = description;
        }
    }
}
