﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repositories
{
    public interface IChildRepository
    {
        int Count();
        Child GetChild(int id);
        void AddChild(string name, string surname, DateTime dateOfBirth, string city, string street);
        void RemoveChild(int id);
        void UpdateChild(Child child);
        IEnumerable<Child> GetAll();


        void AddExpense(Child child);
        void RemoveExpense(Expense expense);
        IList<Expense> GetAllExpenses(int childId);


    }
}
