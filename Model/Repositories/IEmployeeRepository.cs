﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repositories
{
    public interface IEmployeeRepository
    {
        Employee GetEmployee(string email);
        Employee GetEmployee(int id);
        IEnumerable<Employee> GetAll();
        int Count();
        void AddPensioner(int id, string name, string surname, string email, string role, string groupe, string password);
        void RemoveEmployee(int id);
        void UpdateEmployee(Employee employee);
        IList<Purchase> GetPurchases(string email);



    }
}
