﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class EmployeeLog
    {
        public virtual int Id { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Groupa { get; set; }
        public virtual IList<string> Activities { get; set; }
        public virtual IList<string> ProAssociate { get; set; }
        public virtual IList<string> Outcomes { get; set; }
        public virtual string Remark { get; set; }

        public EmployeeLog()
        {
        }

        public EmployeeLog(Employee employee, DateTime date, string groupa)
        {
            Employee = employee;
            Date = date;
            Groupa = groupa;

            Activities = new List<string> { };
            ProAssociate = new List<string> { };
            Outcomes = new List<string> { };
        }

        public EmployeeLog(Employee employee, DateTime date, string groupa, IList<string> activities,
            IList<string> proAssociate, IList<string> outcomes, string remark) : this(employee, date, groupa)
        {
            Activities = activities;
            ProAssociate = proAssociate;
            Outcomes = outcomes;
            Remark = remark;
        }
    }
}
