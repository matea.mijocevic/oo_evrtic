﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Child
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Surname { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual string City { get; set; }
        public virtual string Street { get; set; }


        public virtual IList<string> Alergies { get; set; }
        public virtual IList<Expense> Expenses { get; set; }
        public virtual Parent ParentOne { get; set; }
        public virtual Parent ParentTwo { get; set; }
        public virtual string Groupa { get; set; }
        public virtual IList<string> Remarks { get; set; }

        public Child()
        {
        }

        public Child(string name, string surname, DateTime dateOfBirth, string city, string street, IList<string> alergies,
            IList<Expense> expenses, Parent parentOne, Parent parentTwo, string groupa, 
            IList<string> remarks) : this(name, surname, dateOfBirth, city, street)
        {
            Alergies = alergies;
            Expenses = expenses;
            ParentOne = parentOne;
            ParentTwo = parentTwo;
            Groupa = groupa;
            Remarks = remarks;
        }

        public Child(string name, string surname, DateTime dateOfBirth, string city, string street,
            Parent parentOne, Parent parentTwo, string groupa
            )
        {
            DateTime dt = DateTime.Now;

            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            City = city;
            Street = street;
            Alergies = new List<string> { };
            Expenses = new List<Expense> {new Expense(this, 150, dt.Month, dt.Year, false)};
            ParentOne = parentOne;
            ParentTwo = parentTwo;
            Groupa = groupa; 
            Remarks = new List<string> { };
        }
        //samo da me kontroler ne zeza, kasnije ces maknut ovaj konstruktor
        public Child(string name, string surname, DateTime dateOfBirth, string city, string street)
        {
            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            City = city;
            Street = street;
        }

        public virtual string GetAllUnpayedExpensesString()
        {
            string finalString = "";
            foreach (var x in Expenses)
            {
                if (!x.IsPayed)
                {
                    finalString += x.Amount + "kn, " ;
                }
            }
            return finalString;
        }
    }
}
