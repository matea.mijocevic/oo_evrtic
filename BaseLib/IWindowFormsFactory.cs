﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Model.Repositories;

namespace BaseLib
{
    public interface IWindowFormsFactory
    {

        IEmployeeListForm EmployeeListForm(IEmployeeController employeeController);
        IAboutMeForm AboutMeForm(IEmployeeController employeeController, int id);
        IAddLogForm AddLog(ILogController controller, int id);
        IAddChildForm AddChild(IChildController controller);
        IPurchaseListForm PurchaseListForm(IEmployeeController controller, int id);
        IGeneratePdfForm GeneratePdfForm(IFileController fileController);
        IMyLogListForm MyLogListForm(ILogController controller, int id);
 
    }
}
