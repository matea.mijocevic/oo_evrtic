﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseLib
{
    public static class Enum
    {
        public enum EventType
        {
            Izlet, 
            Roditeljski, 
            Priredba, 
            Drugo
        }

        public enum Role
        {
            Ravnatelj, 
            Odgajatelj
        }

        public enum Groups
        {
            Kikači, 
            Krijesnice, 
            Predškolci
        }
    }
}
