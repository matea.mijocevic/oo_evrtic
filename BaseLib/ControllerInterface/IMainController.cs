﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace BaseLib
{
    public interface IMainController
    {
        void UpdateChildList(IMainForm mainForm);
        Child GetChild(int id); 


        void ShowAboutMeForm(int employeeId);
        void ShowAddLogForm(int employeeId);
        void ShowAddChildForm();
        void ShowEmployeeListForm();
        void ShowGeneratePdfForm();
        void ShowMyLogListForm(int employeeId);
        void ShowPurchaseListForm(int employeeId);
      
    }
}
