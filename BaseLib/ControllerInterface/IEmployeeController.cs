﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace BaseLib
{
    public interface IEmployeeController
    {
        Employee GetEmployee(string email);
        Employee GetEmployee(int id);
        void UpdateEmployeeList(IEmployeeListForm employeeListForm);

    }
}
