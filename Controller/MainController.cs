﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Repositories;
using DataAccessLayer;
using PresentationLayer;
using BaseLib;
using Model;

namespace Controller
{
    public class MainController : IMainController
    {
        private readonly ChildRepository _childRepository = ChildRepository.GetInstance();
        private readonly IWindowFormsFactory _formsFactory;
        private MainForm _mainForm;


        public MainController(IWindowFormsFactory factory)
        {
            _formsFactory = factory;
        }

        public void ShowAboutMeForm(int employeeId)
        {
            var form = _formsFactory.AboutMeForm(new EmployeeController(), employeeId);
            form.ShowForm();
        }

        public void ShowAddChildForm()
        {
            var childController = new ChildController();

            var addChild = _formsFactory.AddChild(childController);

            _childRepository.Attach(_mainForm);

            childController.ShowAddChildForm(addChild);

            _childRepository.Delete(_mainForm);
        }

       
        public void ShowAddLogForm(int employeeId)
        {
            //_formsFactory.AddLog(new LogController(), employeeId).ShowForm();
        }

        public void ShowPurchaseListForm(int employeeId)
        {
            _formsFactory.PurchaseListForm(new EmployeeController(), employeeId).ShowForm();

        }

        public void ShowEmployeeListForm()
        {
            _formsFactory.EmployeeListForm(new EmployeeController()).ShowForm();
        }

        public void ShowGeneratePdfForm()
        {
            var form = _formsFactory.GeneratePdfForm(new FileController());
            form.ShowForm();
        }

        public void ShowMyLogListForm(int employeeId)
        {
            _formsFactory.MyLogListForm(new LogController(), employeeId).ShowForm();
        }


        public void UpdateChildList(IMainForm mainForm)
        {
            _mainForm = (MainForm)mainForm;

            mainForm.UpdateChildListView(_childRepository.GetAll());
        }
       
        public Child GetChild(int id)
        {
           return _childRepository.GetChild(id);
        }
    }
}
