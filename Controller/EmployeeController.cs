﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using Model.Repositories;
using DataAccessLayer;
using Model;
using PresentationLayer;

namespace Controller
{
    public class EmployeeController : IEmployeeController
    {
        private IEmployeeRepository _employeeRepository = EmployeeRepository.GetInstance();
        private readonly IWindowFormsFactory _formsFactory;
        private EmployeeListForm _form;

        public Employee GetEmployee(string email)
        {
            IEnumerable<Employee> eList = _employeeRepository.GetAll();

            Employee employee = null;
            foreach(var x in eList)
            {
                if(x.Email == email)
                {
                    employee = x;
                    break;
                }
            }

            return _employeeRepository.GetEmployee(email);
        }

        public Employee GetEmployee(int id)
        {
            return _employeeRepository.GetEmployee(id);
        }

        public void UpdateEmployeeList(IEmployeeListForm employeeListForm)
        {
            _form = (EmployeeListForm)employeeListForm;

            employeeListForm.UpdateEmployeeListForm(_employeeRepository.GetAll());
        }
    }
}
