﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using Model;

using System.Diagnostics.Eventing.Reader;
using System.Windows.Forms;
using DataAccessLayer;
using Model.Repositories;
using PresentationLayer;


namespace Controller
{
    public class ChildController : IChildController
    {
        public Child GetChild(int id)
        {
            throw new NotImplementedException();
        }

        public void ShowAddChildForm(IAddChildForm addChildForm)
        {
            if (addChildForm.ShowAddChildDialog())
            {
                MessageBox.Show("Član uspješno spremljen!");
            }
        }
    }
}
