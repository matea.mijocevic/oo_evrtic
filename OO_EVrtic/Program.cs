﻿using System;
using System.Windows.Forms;
using Controller;
using PresentationLayer;


namespace OO_EVrtic
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm( new MainForm(new MainController(new WindowsFormsFactory())), new EmployeeController()));
        }
    }
}
