﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;
using Model;

namespace PresentationLayer
{
    public partial class AboutMeForm : Form, IAboutMeForm
    {

        private readonly IEmployeeController _controller;
        private readonly Employee employee;
        public AboutMeForm(IEmployeeController controller, int id)
        {
            _controller = controller;
            employee = _controller.GetEmployee(id);
            InitializeComponent();
        }

        public void ShowForm()
        {
            ShowDialog();
        }

        private void AboutMeForm_Load(object sender, EventArgs e)
        {
            label6.Text = employee.Name;
            label7.Text = employee.Surname;
            label8.Text = employee.Groupa;
            label9.Text = employee.Email;
           
        }
    }
}
