﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;
using Model;

namespace PresentationLayer
{
    public partial class MyLogListForm : Form, IMyLogListForm
    {
        private readonly ILogController _controller;
        private readonly Employee employee;
        public MyLogListForm(ILogController controller, int id)
        {
            _controller = controller;
//            employee = _controller.Get(id);
            InitializeComponent();
        }

        public void ShowForm()
        {
            ShowDialog();
        }
    }
}
