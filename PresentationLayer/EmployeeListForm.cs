﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;
using Model;

namespace PresentationLayer
{
    public partial class EmployeeListForm : Form, IEmployeeListForm
    {
        private readonly IEmployeeController _controller;

        public EmployeeListForm(IEmployeeController employeeController)
        {
            _controller = employeeController;
            InitializeComponent();
            UpdateView();
        }
        public void UpdateView()
        {
            _controller.UpdateEmployeeList(this);
        }

        public void ShowForm()
        {
            ShowDialog();
        }

        private void AddEmployeeForm_Load(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void UpdateEmployeeListForm(IEnumerable<Employee> employees)
        {
            listView1.Items.Clear();

            foreach (var em in employees)
            {
                ListViewItem listViewItem = new ListViewItem(em.Id.ToString());
                listViewItem.SubItems.Add(em.Surname);
                listViewItem.SubItems.Add(em.Name);
                listViewItem.SubItems.Add(em.Email);

                listViewItem.SubItems.Add(em.Groupa);

                listView1.Items.Add(listViewItem);
            }
        }
    }
}
