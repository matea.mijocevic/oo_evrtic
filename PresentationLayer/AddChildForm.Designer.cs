﻿namespace PresentationLayer
{
    partial class AddChildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Ime = new System.Windows.Forms.TextBox();
            this.Prezime = new System.Windows.Forms.TextBox();
            this.Grad = new System.Windows.Forms.TextBox();
            this.Ulica = new System.Windows.Forms.TextBox();
            this.Alergije = new System.Windows.Forms.TextBox();
            this.Napomene = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Grupa = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TelefonP1 = new System.Windows.Forms.TextBox();
            this.EmailP1 = new System.Windows.Forms.TextBox();
            this.PrezimeP1 = new System.Windows.Forms.TextBox();
            this.ImeP1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.PrezimeP2 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ImeP2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.EmailP2 = new System.Windows.Forms.TextBox();
            this.TelefonP2 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Datum rođenja";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(105, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Grad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ulica i kućni broj";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Alergije";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(94, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Grupa";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(63, 294);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Napomene";
            // 
            // Ime
            // 
            this.Ime.Location = new System.Drawing.Point(155, 31);
            this.Ime.Name = "Ime";
            this.Ime.Size = new System.Drawing.Size(240, 26);
            this.Ime.TabIndex = 8;
            // 
            // Prezime
            // 
            this.Prezime.Location = new System.Drawing.Point(155, 61);
            this.Prezime.Name = "Prezime";
            this.Prezime.Size = new System.Drawing.Size(240, 26);
            this.Prezime.TabIndex = 9;
            // 
            // Grad
            // 
            this.Grad.Location = new System.Drawing.Point(155, 124);
            this.Grad.Name = "Grad";
            this.Grad.Size = new System.Drawing.Size(240, 26);
            this.Grad.TabIndex = 10;
            // 
            // Ulica
            // 
            this.Ulica.Location = new System.Drawing.Point(155, 157);
            this.Ulica.Name = "Ulica";
            this.Ulica.Size = new System.Drawing.Size(240, 26);
            this.Ulica.TabIndex = 11;
            // 
            // Alergije
            // 
            this.Alergije.Location = new System.Drawing.Point(154, 224);
            this.Alergije.Multiline = true;
            this.Alergije.Name = "Alergije";
            this.Alergije.Size = new System.Drawing.Size(239, 59);
            this.Alergije.TabIndex = 12;
            // 
            // Napomene
            // 
            this.Napomene.Location = new System.Drawing.Point(153, 289);
            this.Napomene.Multiline = true;
            this.Napomene.Name = "Napomene";
            this.Napomene.Size = new System.Drawing.Size(240, 63);
            this.Napomene.TabIndex = 13;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(155, 91);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(240, 26);
            this.dateTimePicker1.TabIndex = 14;
            // 
            // Grupa
            // 
            this.Grupa.FormattingEnabled = true;
            this.Grupa.Items.AddRange(new object[] {
            "Kikači",
            "Krijesnice",
            "Predškolci"});
            this.Grupa.Location = new System.Drawing.Point(154, 190);
            this.Grupa.Name = "Grupa";
            this.Grupa.Size = new System.Drawing.Size(239, 28);
            this.Grupa.TabIndex = 17;
            this.Grupa.SelectedIndexChanged += new System.EventHandler(this.Grupa_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(330, 371);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 50);
            this.button1.TabIndex = 18;
            this.button1.Text = "Dodaj";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.TelefonP1);
            this.panel1.Controls.Add(this.EmailP1);
            this.panel1.Controls.Add(this.PrezimeP1);
            this.panel1.Controls.Add(this.ImeP1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Location = new System.Drawing.Point(441, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(347, 152);
            this.panel1.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 20);
            this.label14.TabIndex = 8;
            this.label14.Text = "Telefon";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 20);
            this.label13.TabIndex = 7;
            this.label13.Text = "Email";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 20);
            this.label12.TabIndex = 6;
            this.label12.Text = "Prezime";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(33, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 20);
            this.label11.TabIndex = 5;
            this.label11.Text = "Ime";
            // 
            // TelefonP1
            // 
            this.TelefonP1.Location = new System.Drawing.Point(75, 119);
            this.TelefonP1.Name = "TelefonP1";
            this.TelefonP1.Size = new System.Drawing.Size(258, 26);
            this.TelefonP1.TabIndex = 4;
            // 
            // EmailP1
            // 
            this.EmailP1.Location = new System.Drawing.Point(75, 89);
            this.EmailP1.Name = "EmailP1";
            this.EmailP1.Size = new System.Drawing.Size(258, 26);
            this.EmailP1.TabIndex = 3;
            // 
            // PrezimeP1
            // 
            this.PrezimeP1.Location = new System.Drawing.Point(75, 59);
            this.PrezimeP1.Name = "PrezimeP1";
            this.PrezimeP1.Size = new System.Drawing.Size(258, 26);
            this.PrezimeP1.TabIndex = 2;
            // 
            // ImeP1
            // 
            this.ImeP1.Location = new System.Drawing.Point(75, 30);
            this.ImeP1.Name = "ImeP1";
            this.ImeP1.Size = new System.Drawing.Size(258, 26);
            this.ImeP1.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Roditelj1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.PrezimeP2);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.ImeP2);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.EmailP2);
            this.panel2.Controls.Add(this.TelefonP2);
            this.panel2.Location = new System.Drawing.Point(441, 194);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(347, 158);
            this.panel2.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 126);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 20);
            this.label15.TabIndex = 16;
            this.label15.Text = "Telefon";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(-4, -1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "Roditelj2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(21, 96);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 20);
            this.label16.TabIndex = 15;
            this.label16.Text = "Email";
            // 
            // PrezimeP2
            // 
            this.PrezimeP2.Location = new System.Drawing.Point(75, 63);
            this.PrezimeP2.Name = "PrezimeP2";
            this.PrezimeP2.Size = new System.Drawing.Size(258, 26);
            this.PrezimeP2.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 66);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 20);
            this.label17.TabIndex = 14;
            this.label17.Text = "Prezime";
            // 
            // ImeP2
            // 
            this.ImeP2.Location = new System.Drawing.Point(75, 34);
            this.ImeP2.Name = "ImeP2";
            this.ImeP2.Size = new System.Drawing.Size(258, 26);
            this.ImeP2.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(33, 34);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 20);
            this.label18.TabIndex = 13;
            this.label18.Text = "Ime";
            // 
            // EmailP2
            // 
            this.EmailP2.Location = new System.Drawing.Point(75, 93);
            this.EmailP2.Name = "EmailP2";
            this.EmailP2.Size = new System.Drawing.Size(258, 26);
            this.EmailP2.TabIndex = 11;
            this.EmailP2.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // TelefonP2
            // 
            this.TelefonP2.Location = new System.Drawing.Point(75, 123);
            this.TelefonP2.Name = "TelefonP2";
            this.TelefonP2.Size = new System.Drawing.Size(258, 26);
            this.TelefonP2.TabIndex = 12;
            // 
            // AddChildForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Grupa);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.Napomene);
            this.Controls.Add(this.Alergije);
            this.Controls.Add(this.Ulica);
            this.Controls.Add(this.Grad);
            this.Controls.Add(this.Prezime);
            this.Controls.Add(this.Ime);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddChildForm";
            this.Text = "AddChildForm";
            this.Load += new System.EventHandler(this.AddChildForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Ime;
        private System.Windows.Forms.TextBox Prezime;
        private System.Windows.Forms.TextBox Grad;
        private System.Windows.Forms.TextBox Ulica;
        private System.Windows.Forms.TextBox Alergije;
        private System.Windows.Forms.TextBox Napomene;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox Grupa;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TelefonP1;
        private System.Windows.Forms.TextBox EmailP1;
        private System.Windows.Forms.TextBox PrezimeP1;
        private System.Windows.Forms.TextBox ImeP1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox PrezimeP2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ImeP2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox EmailP2;
        private System.Windows.Forms.TextBox TelefonP2;
    }
}