﻿namespace PresentationLayer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.grupeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nabavkaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dnevnikRadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajDnevnikRadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sviMojiDnevniciRadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zaposleniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvješćeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generirajIzvješćeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odjavaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grupeToolStripMenuItem,
            this.nabavkaToolStripMenuItem,
            this.dnevnikRadaToolStripMenuItem,
            this.zaposleniciToolStripMenuItem,
            this.izvješćeToolStripMenuItem,
            this.odjavaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(960, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // grupeToolStripMenuItem
            // 
            this.grupeToolStripMenuItem.Name = "grupeToolStripMenuItem";
            this.grupeToolStripMenuItem.Size = new System.Drawing.Size(115, 29);
            this.grupeToolStripMenuItem.Text = "Upiši dijete";
            this.grupeToolStripMenuItem.Click += new System.EventHandler(this.grupeToolStripMenuItem_Click);
            // 
            // nabavkaToolStripMenuItem
            // 
            this.nabavkaToolStripMenuItem.Name = "nabavkaToolStripMenuItem";
            this.nabavkaToolStripMenuItem.Size = new System.Drawing.Size(97, 29);
            this.nabavkaToolStripMenuItem.Text = "Nabavke";
            this.nabavkaToolStripMenuItem.Click += new System.EventHandler(this.nabavkaToolStripMenuItem_Click);
            // 
            // dnevnikRadaToolStripMenuItem
            // 
            this.dnevnikRadaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajDnevnikRadaToolStripMenuItem,
            this.sviMojiDnevniciRadaToolStripMenuItem});
            this.dnevnikRadaToolStripMenuItem.Name = "dnevnikRadaToolStripMenuItem";
            this.dnevnikRadaToolStripMenuItem.Size = new System.Drawing.Size(173, 29);
            this.dnevnikRadaToolStripMenuItem.Text = "Moji dnevnici rada";
            // 
            // dodajDnevnikRadaToolStripMenuItem
            // 
            this.dodajDnevnikRadaToolStripMenuItem.Name = "dodajDnevnikRadaToolStripMenuItem";
            this.dodajDnevnikRadaToolStripMenuItem.Size = new System.Drawing.Size(287, 34);
            this.dodajDnevnikRadaToolStripMenuItem.Text = "Dodaj dnevnik rada";
            this.dodajDnevnikRadaToolStripMenuItem.Click += new System.EventHandler(this.dodajDnevnikRadaToolStripMenuItem_Click);
            // 
            // sviMojiDnevniciRadaToolStripMenuItem
            // 
            this.sviMojiDnevniciRadaToolStripMenuItem.Name = "sviMojiDnevniciRadaToolStripMenuItem";
            this.sviMojiDnevniciRadaToolStripMenuItem.Size = new System.Drawing.Size(287, 34);
            this.sviMojiDnevniciRadaToolStripMenuItem.Text = "Svi moji dnevnici rada";
            this.sviMojiDnevniciRadaToolStripMenuItem.Click += new System.EventHandler(this.sviMojiDnevniciRadaToolStripMenuItem_Click);
            // 
            // zaposleniciToolStripMenuItem
            // 
            this.zaposleniciToolStripMenuItem.Name = "zaposleniciToolStripMenuItem";
            this.zaposleniciToolStripMenuItem.Size = new System.Drawing.Size(116, 29);
            this.zaposleniciToolStripMenuItem.Text = "Zaposlenici";
            this.zaposleniciToolStripMenuItem.Click += new System.EventHandler(this.zaposleniciToolStripMenuItem_Click);
            // 
            // izvješćeToolStripMenuItem
            // 
            this.izvješćeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generirajIzvješćeToolStripMenuItem});
            this.izvješćeToolStripMenuItem.Name = "izvješćeToolStripMenuItem";
            this.izvješćeToolStripMenuItem.Size = new System.Drawing.Size(88, 29);
            this.izvješćeToolStripMenuItem.Text = "Izvješće";
            // 
            // generirajIzvješćeToolStripMenuItem
            // 
            this.generirajIzvješćeToolStripMenuItem.Name = "generirajIzvješćeToolStripMenuItem";
            this.generirajIzvješćeToolStripMenuItem.Size = new System.Drawing.Size(247, 34);
            this.generirajIzvješćeToolStripMenuItem.Text = "Generiraj izvješće";
            this.generirajIzvješćeToolStripMenuItem.Click += new System.EventHandler(this.generirajIzvješćeToolStripMenuItem_Click);
            // 
            // odjavaToolStripMenuItem
            // 
            this.odjavaToolStripMenuItem.Name = "odjavaToolStripMenuItem";
            this.odjavaToolStripMenuItem.Size = new System.Drawing.Size(84, 29);
            this.odjavaToolStripMenuItem.Text = "Odjava";
            this.odjavaToolStripMenuItem.Click += new System.EventHandler(this.odjavaToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(712, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Djeca";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(26, 82);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(909, 346);
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Id";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Prezime";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Ime";
            this.columnHeader3.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Datum rođenja";
            this.columnHeader4.Width = 150;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Grupa";
            this.columnHeader5.Width = 100;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 450);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nabavkaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dnevnikRadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zaposleniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvješćeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odjavaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grupeToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem dodajDnevnikRadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sviMojiDnevniciRadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generirajIzvješćeToolStripMenuItem;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
    }
}