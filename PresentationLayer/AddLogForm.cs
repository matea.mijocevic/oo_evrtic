﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;

namespace PresentationLayer
{
    public partial class AddLogForm : Form, IAddLogForm
    {
        public AddLogForm()
        {
            InitializeComponent();
        }

        public void ShowForm()
        {
            ShowDialog();

        }

        private void AddLogForm_Load(object sender, EventArgs e)
        {

        }
    }
}
