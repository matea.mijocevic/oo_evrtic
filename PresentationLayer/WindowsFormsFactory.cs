﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseLib;
using Model;
using Model.Repositories;

namespace PresentationLayer
{
    public class WindowsFormsFactory : IWindowFormsFactory
    {
        public IAboutMeForm AboutMeForm(IEmployeeController employeeController, int id)
        {
            return new AboutMeForm(employeeController, id);
        }
        public IAddChildForm AddChild(IChildController childController)
        {
            return new AddChildForm(childController);
        }
        public IAddLogForm AddLog(ILogController logController, int id)
        {
            //jel ovdje saljem log kontroler ili employee
            return null;// new AddLogForm(logController, id);
        }
        public IEmployeeListForm EmployeeListForm(IEmployeeController employeeController)
        {
            return new EmployeeListForm(employeeController);
        }
        public IGeneratePdfForm GeneratePdfForm(IFileController fileController)
        {
            return new GeneratePdfForm(fileController);
        }
        public IMyLogListForm MyLogListForm(ILogController logController, int id)
        {
            return new MyLogListForm(logController, id);
        }
        public IPurchaseListForm PurchaseListForm(IEmployeeController employeeController, int id)
        {
            return new PurchaseListForm(employeeController, id);
        }
    }
}
