﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;

namespace PresentationLayer
{
    public partial class AddChildForm : Form, IAddChildForm
    {
        private readonly IChildController _controller;

        public AddChildForm(IChildController controller)
        {
            _controller = controller;
            InitializeComponent();
        }

        public bool ShowAddChildDialog()
        {
            return ShowDialog() == DialogResult.OK;
        }

        private void AddChildForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!ValidateChildren()) return;
            var name = Ime.Text.Trim();
            var surname = Prezime.Text.Trim();
            var dateOfBirth = dateTimePicker1.Value;
            var town = Grad.Text.Trim();
            var street = Ulica.Text.Trim();
            var groupe = Grupa.SelectedItem;
            var alergies = Alergije.Text.Trim();
            var remarks = Napomene.Text.Trim();

            //Parent1
            var nameP1 = ImeP1.Text.Trim();
            var surnameP1 = PrezimeP1.Text.Trim();
            var emailP1 = EmailP1.Text.Trim();
            var phoneP1 = TelefonP1.Text.Trim();

            //Parent2
            var nameP2 = ImeP2.Text.Trim();
            var surnameP2 = PrezimeP2.Text.Trim();
            var emailP2 = EmailP2.Text.Trim();
            var phoneP2 = TelefonP2.Text.Trim();

         //   _controller.AddChild();
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void Grupa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
