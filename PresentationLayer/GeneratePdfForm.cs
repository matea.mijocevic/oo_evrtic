﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;

namespace PresentationLayer
{
    public partial class GeneratePdfForm : Form, IGeneratePdfForm
    {
        private readonly IFileController _fileController;
        public GeneratePdfForm(IFileController fileController)
        {
            _fileController = fileController;
            InitializeComponent();
        }

        public void ShowForm()
        {
            ShowDialog();
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
       
            _fileController.CreateFile();
        }

        private void GeneratePdfForm_Load(object sender, EventArgs e)
        {

        }
    }
}
