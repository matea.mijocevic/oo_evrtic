﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;
using Model;

namespace PresentationLayer
{
    public partial class PurchaseListForm : Form, IPurchaseListForm
    {
        private readonly IEmployeeController _controller;
        private readonly Employee _employee;
        public PurchaseListForm(IEmployeeController controller, int id)
        {
            _controller = controller;
        //    _employee = _controller.GetEmployee(id);

            InitializeComponent();

        }

        public void ShowForm()
        {
            ShowDialog();
        }
    }
}
