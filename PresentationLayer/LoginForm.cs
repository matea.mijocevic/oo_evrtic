﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseLib;
using Model;

namespace PresentationLayer
{
    public partial class LoginForm : Form
    {
        MainForm _mainForm;
        private readonly IEmployeeController _controller;
        public static Employee _employee;

        public LoginForm(MainForm mainForm, IEmployeeController employeeController)
        {
            _mainForm = mainForm;
            _controller = employeeController;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        public static string Email = "";
        public static string Pass = "";

        private void button1_Click(object sender, EventArgs e)
        {

            Email = textBox1.Text;
            Pass = textBox2.Text;
            if (Email != "" && Pass != "")
            {
                if (_controller.GetEmployee(Email) != null && _controller.GetEmployee(Email).Password == Pass)
                {
                    _employee = _controller.GetEmployee(Email);
                    _mainForm.Show();
                    this.Hide();
                }
                else
                    MessageBox.Show("Unijeli ste neispravne podatke za prijavu.");
                
            }
            else
            {
                MessageBox.Show("Sva polja moraju biti ispunjena");
            }
        }
    }
}
