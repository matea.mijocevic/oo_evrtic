﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BaseLib;
using Model;

namespace PresentationLayer
{
    public partial class MainForm : Form, IMainForm, IObserver
    {
        private readonly IMainController _controller;

        public MainForm()
        {
            InitializeComponent();
        }
        public MainForm(IMainController mainController)
        {
            _controller = mainController;

            InitializeComponent();

            UpdateView();
        }


        public void UpdateView()
        {
            _controller.UpdateChildList(this);
        }


        private void dodajNabavkuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            _controller.ShowAboutMeForm(LoginForm._employee.Id);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
           label1.Text = "(" + LoginForm._employee.Name + " " + LoginForm._employee.Surname + ")";
        }

        private void zaposleniciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowEmployeeListForm();
        }

        private void odjavaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new LoginForm(this, null).Show();
            this.Hide();
        }


        public void UpdateChildListView(IEnumerable<Child> children)
        {
            listView1.Items.Clear();

            foreach (var child in children)
            {
                ListViewItem listViewItem = new ListViewItem(child.Id.ToString());
                listViewItem.SubItems.Add(child.Surname);
                listViewItem.SubItems.Add(child.Name);
                listViewItem.SubItems.Add(child.DateOfBirth.Day.ToString()+"."+ child.DateOfBirth.Month.ToString()+"."+ child.DateOfBirth.Year.ToString()+".");

                listViewItem.SubItems.Add(child.Groupa);
                //listViewItem.SubItems.Add((child.Remarks).ToString());
                //listViewItem.SubItems.Add((child.Alergies).ToString()); //-> zatvorena sesija 
                // listViewItem.SubItems.Add(child.ParentOne.Name);
                //listViewItem.SubItems.Add(child.ParentOne.Surname);

                 listView1.Items.Add(listViewItem);
            }
        }

        private void dodajDnevnikRadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowAddLogForm(LoginForm._employee.Id);
        }

        private void sviMojiDnevniciRadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowMyLogListForm(LoginForm._employee.Id);
        }

        private void generirajIzvješćeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowGeneratePdfForm();
        }

        private void grupeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowAddChildForm();
        }

        private void nabavkaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _controller.ShowPurchaseListForm(LoginForm._employee.Id);
        }
    }
}
