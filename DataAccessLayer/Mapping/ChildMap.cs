﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    public class ChildMap : ClassMap<Child>
    {

        public ChildMap()
        {
            Id(x => x.Id).Not.Nullable().Unique();
            LazyLoad();
            References(x => x.ParentOne);
            References(x => x.ParentTwo);
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Surname).Not.Nullable();
            Map(x => x.DateOfBirth).Not.Nullable().CustomType<NHibernate.Type.DateTimeType>();
            Map(x => x.City).Not.Nullable();
            Map(x => x.Street).Not.Nullable();
            Map(x => x.Groupa).Not.Nullable();
            HasMany<string>(x => x.Remarks).Element("Value");
            HasMany<string>(x => x.Alergies).Element("Value");
            HasMany(x => x.Expenses).Inverse().Cascade.AllDeleteOrphan();
            Table("Child");

        }
    }
}
