﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    public class ParentMap : ClassMap<Parent>
    {
        public ParentMap()
        {
            Id(x => x.Id).Not.Nullable().Unique();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Surname).Not.Nullable();
            Map(x => x.Email).Not.Nullable();
            Map(x => x.PhoneNumber).Not.Nullable();
            HasMany(x => x.Children).Inverse().Cascade.AllDeleteOrphan();
        }
    }
}
