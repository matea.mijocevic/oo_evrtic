﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    class EventMap : ClassMap<Event>
    {
        public EventMap()
        {
            Id(x => x.Id).Not.Nullable().Unique();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.EventType).Not.Nullable();
            HasMany<string>(x => x.Groups).Element("Value").Not.KeyNullable();
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.About);
            Map(x => x.Address).Not.Nullable();
            Map(x => x.StartingDate).Not.Nullable().CustomType<NHibernate.Type.DateTimeType>(); ;
            Map(x => x.EndingDate).Not.Nullable().CustomType<NHibernate.Type.DateTimeType>(); ;

        }
    }
}
