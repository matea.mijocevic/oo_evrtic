﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    class EmployeeLogMap : ClassMap<EmployeeLog>
    {
        public EmployeeLogMap()
        {

            Id(x => x.Id).Not.Nullable().Unique();
            LazyLoad();
            References(x => x.Employee);
            Map(x => x.Date).Not.Nullable().CustomType<NHibernate.Type.DateTimeType>(); ;
            Map(x => x.Groupa).Not.Nullable();
            HasMany<string>(x => x.Activities).Element("Value");
            HasMany<string>(x => x.ProAssociate).Element("Value");
            HasMany<string>(x => x.Outcomes).Element("Value");
            Map(x => x.Remark);
            
        }
    }
}
