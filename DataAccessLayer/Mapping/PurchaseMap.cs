﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    public class PurchaseMap : ClassMap<Purchase>
    {
        public PurchaseMap()
        {
            Id(x => x.Id).Not.Nullable().Unique();
            LazyLoad();
            References(x => x.Employee);
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.Description);
         
        }
    }
}
