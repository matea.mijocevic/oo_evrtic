﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    class ExpenseMap : ClassMap<Expense>
    {
        public ExpenseMap()
        {

            Id(x => x.Id).Not.Nullable().Unique();
            LazyLoad();
            References(x => x.Child);
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.Month).Not.Nullable();
            Map(x => x.Year).Not.Nullable();
            Map(x => x.IsPayed).Not.Nullable();
        }
    }
}
