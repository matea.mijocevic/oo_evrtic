﻿using FluentNHibernate.Mapping;
using Model;

namespace DataAccessLayer.Mapping
{
    class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {

            Id(x => x.Id).Not.Nullable().Unique();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Surname).Not.Nullable();
            Map(x => x.Groupa).Not.Nullable();
            Map(x => x.Email).Not.Nullable();
            Map(x => x.Password).Not.Nullable();
            HasMany(x => x.EmployeeLogs).Inverse().Cascade.AllDeleteOrphan();
            HasMany(x => x.Purchases).Inverse().Cascade.AllDeleteOrphan();

        }
    }
}
