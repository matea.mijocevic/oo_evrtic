﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Repositories;
using Model;
using BaseLib;
using NHibernate;
using NHibernate.Criterion;

namespace DataAccessLayer
{
    public class EmployeeRepository : Subject, IEmployeeRepository
    {
        private static EmployeeRepository _instance;

        private ISession Session => TestSessionFactory != null ? TestSessionFactory.OpenSession() : NHibernateService.OpenSession();
        private readonly ISessionFactory TestSessionFactory;

        //Only for testing purposes
        private EmployeeRepository(ISessionFactory sessionFactory)
        {
            TestSessionFactory = sessionFactory;
        }

        //Only for testing purposes
        public static EmployeeRepository GetInstanceForTesting(ISessionFactory factory)
        {
            return _instance ?? (_instance = new EmployeeRepository(factory));
        }

        private EmployeeRepository()
        {
        }
        public static EmployeeRepository GetInstance()
        {
            return _instance ?? (_instance = new EmployeeRepository());
        }

        public Employee GetEmployee(string email)
        {
            using (var session = Session)
            {

                return session.Query<Employee>().Where(c => c.Email.Equals(email)).FirstOrDefault();
              
            }
        }
        public Employee GetEmployee(int id)
        {
            using (var session = Session)
            {

                return session.Get<Employee>(id);

            }
        }


        public IEnumerable<Employee> GetAll()
        {
            using (var session = Session)
            {
                return session.Query<Employee>().ToList();
            }
        }

        public int Count()
        {
            using (var session = Session)
            {
                return session.Query<Employee>().Count();
            }
        }

        public void AddPensioner(int id, string name, string surname, string email, string role, string groupe, string password)
        {

        }
        public void RemoveEmployee(int id)
        {

        }
        public void UpdateEmployee(Employee employee)
        {

        }
        public IList<Purchase> GetPurchases(string email)
        {
            return null;
        }
    }
}
