﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using BaseLib;
using FluentNHibernate.Conventions;
using Model;
using Model.Repositories;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;


namespace DataAccessLayer
{
    public class ChildRepository : Subject, IChildRepository
    {
        private static ChildRepository _instance;
        private ISession Session => TestSessionFactory != null ? TestSessionFactory.OpenSession() : NHibernateService.OpenSession();
        private readonly ISessionFactory TestSessionFactory;

        private ChildRepository()
        {
        }

        public static ChildRepository GetInstance()
        {
            return _instance ?? (_instance = new ChildRepository());
        }

        public void AddChild(string name, string surname, DateTime dateOfBirth, string city, string street)
        {
            using (var session = Session)
            {
                var transaction = session.BeginTransaction();

                var child = new Child(name, surname, dateOfBirth, city, street);

                session.Save(child);
                transaction.Commit();
            }

            NotifyObservers();
        }

        public int Count()
        {
            using (var session = Session)
            {
                return session.Query<Child>().Count();
            }
        }

        public IEnumerable<Child> GetAll()
        {
            using (var session = Session)
            {
                return session.Query<Child>().ToList();
            }
        }

        public Child GetChild(int id)
        {
            using (var session = Session)
            {
                return session.Get<Child>(id);
            }
        }

        public void RemoveChild(int id)
        {
            using (var session = Session)
            {
                var transaction = session.BeginTransaction();
                var child = session.Get<Child>(id);

                session.Delete(child);
                transaction.Commit();
            }

            NotifyObservers();
        }

        public void UpdateChild(Child child)
        {
            using (var session = Session)
            {
                var transaction = session.BeginTransaction();

                session.Update(child);

                transaction.Commit();
            }

            NotifyObservers();
        }

        public void AddExpense(Child child)
        {
            throw new NotImplementedException();
        }

        public void RemoveExpense(Expense expense)
        {
            using (var session = Session)
            {
                var transaction = session.BeginTransaction();

                session.Delete(expense);

                transaction.Commit();
            }

            NotifyObservers();
        }

        public IList<Expense> GetAllExpenses(int childId)
        {
            using (var session = Session)
            {
                return session.Query<Expense>().Where(c => c.Child.Id.Equals(childId)).ToList();
            }
        }
    }
}
